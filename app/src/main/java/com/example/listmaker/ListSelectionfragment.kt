package com.example.listmaker

import android.content.Context
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView


class ListSelectionfragment : Fragment(), ListSelectionListener {

    private var listener: OnFragmentInteractionListener? = null

    lateinit var listsRecyclerView: RecyclerView
    lateinit var listDataManager: ListDataManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_list_selectionfragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val lists = listDataManager.readLists()


        view?.let { //si existe la view, entonces ejecuta lo de adentro
            listsRecyclerView = it.findViewById(R.id.lists_recycleview)
            listsRecyclerView.layoutManager = LinearLayoutManager(activity) //
            listsRecyclerView.adapter = ListsRecycleViewAdapter(lists, this)
        }


    }

    override fun listItemSelected(list: TaskList) {
        listener?.onListItemSelected(list)
    }



    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
            listDataManager = ListDataManager(context)
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnFragmentInteractionListener {
        fun onListItemSelected(list: TaskList) //funcion que ayuda a coimunicarse con el activity
    }

    fun addList(list: TaskList){
        listDataManager.saveList(list)

        val adapter= listsRecyclerView.adapter as ListsRecycleViewAdapter
        adapter.addList(list)
    }

    fun saveList(list: TaskList){
        listDataManager.saveList(list)
        updateLists()
    }


    private fun updateLists(){
        val lists = listDataManager.readLists()
        listsRecyclerView.adapter = ListsRecycleViewAdapter(lists, this)
    }


    companion object {
        fun newInstance(): ListSelectionfragment{
            val fragment = ListSelectionfragment()
            return fragment
        }
    }
}
