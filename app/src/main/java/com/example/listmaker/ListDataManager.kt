package com.example.listmaker

import android.content.Context
import androidx.preference.PreferenceManager


class ListDataManager (private  val context: Context){

    fun saveList(list: TaskList){
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context).edit()//creo una instancia de sharedPreferences llave nombre de la lista, valor todas las entradas de la lista
        sharedPreferences.putStringSet(list.name, list.tasks.toHashSet()) //nombre de lista como llave y el resto de tareas como valor
        sharedPreferences.apply() //como estamos haciendo un edit del shared prefences, se debe realizar un apply a los cambios
    }
    //para leer lo que se tiene guardado en el shared preference
    fun  readLists(): ArrayList<TaskList>{
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context) //necesito una instancia de sharedPreferences
        val sharedPreferencesContents = sharedPreferences.all

        val taskLists = ArrayList<TaskList>() //se van a añadir las tareas vacias

        for ( taskList in sharedPreferencesContents){
            val itemsHash = ArrayList(taskList.value as HashSet<String>)
            val list = TaskList(taskList.key, itemsHash)

            taskLists.add(list)
        }
        return taskLists
    }
}