package com.example.listmaker

import android.os.Parcel
import android.os.Parcelable

class TaskList (val name:String, val tasks: ArrayList<String> = ArrayList()):Parcelable{
    constructor(parcel: Parcel) : this(
        //LOS SIGUIENTES SON OPCIONALES (es decir que son tipos opcionales de algun tipo de dato)
        parcel.readString() ?: "Generic List", //se espera un String, pero como es un opcional sale un error
        // si es: !! --> le decimos al compilador que estamos seguros q se va a pasar un string
        // si es: ?: --> le decimos al compilador que si no es string, entonces devuelva un string llamado "Generic List"
        parcel.createStringArrayList() ?: ArrayList()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeStringList(tasks)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<TaskList> {
        override fun createFromParcel(parcel: Parcel): TaskList {
            return TaskList(parcel)
        }

        override fun newArray(size: Int): Array<TaskList?> {
            return arrayOfNulls(size)
        }
    }
}