package com.example.listmaker

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView


//Patrón de diseño delegado

interface ListSelectionListener{
    fun listItemSelected (list: TaskList)
}

class ListsRecycleViewAdapter(
    private val lists: ArrayList<TaskList> = ArrayList(),
    val selectionListener: ListSelectionListener): RecyclerView.Adapter<ListViewHolder>(){

    //val mainList = arrayOf("Shopping List", "Homework", "Chores")

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.view_holder, parent, false)
        return ListViewHolder(view) //LayoutInflater permite crear una vista que permita representar al view holder creado en el layout
    }

    override fun getItemCount(): Int {
        return lists.size
    }

    fun addList(list: TaskList){
        lists.add(list)
        notifyItemInserted(lists.size - 1)
    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        holder.listItemId.text = (position + 1).toString()
        holder.listItemTitle.text = lists[position].name

        holder.itemView.setOnClickListener{selectionListener.listItemSelected(lists[position])
        }
    }
}
