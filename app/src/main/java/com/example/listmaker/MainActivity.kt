package com.example.listmaker

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.InputType
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.widget.EditText
import android.widget.FrameLayout
import androidx.appcompat.app.AlertDialog

import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), ListSelectionfragment.OnFragmentInteractionListener {

    private var listSelectionFragment = ListSelectionfragment.newInstance()
    private var fragmentContainer: FrameLayout? = null

    private var largeScreen = false
    private var listFragment: ListDetailFragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        listSelectionFragment = supportFragmentManager.findFragmentById(R.id.list_selection_fragment) as ListSelectionfragment

        fragmentContainer = findViewById(R.id.fragment_container) //trato de encontrar una vista o un fragment de nombre fragment container

        largeScreen = fragmentContainer != null //si es diferente de null, entonces estamos un una oaantalla gtrande

        fab.setOnClickListener { view ->
            showDialog()
        }
    }

    fun showDialog (){
        var builder = AlertDialog.Builder(this)
        var listTitleEditText = EditText(this)
        listTitleEditText.inputType = InputType.TYPE_CLASS_TEXT //doy una pista de que tipo de teclado quiero mostrar

        builder.setTitle(getString(R.string.enter_list_name))
        builder.setView(listTitleEditText) //agrego una vista

        builder.setPositiveButton(getString(R.string.add_list)){ dialog, _ -> dialog.dismiss()
            val list = TaskList(listTitleEditText.text.toString())

            listSelectionFragment.addList(list)

            dialog.dismiss()
            showListDetails(list)

        }
        //tambien se pueden añadir negative buttons, la diferencia sera el UI y como se ven los dos
        builder.setNegativeButton("NEGATIVE"){ dialog, _ ->

            dialog.dismiss()
        }
        builder.create().show()
    }

    override fun onListItemSelected(list: TaskList) {
        showListDetails(list)
    }

    private fun showListDetails(list: TaskList) {

        if (!largeScreen) {
            val lisDetailIntent = Intent (this, ListDetailActivity::class.java)
            lisDetailIntent.putExtra(INTENT_LIST_KEY, list)

            startActivityForResult(lisDetailIntent, LIST_DETAIL_ACTIVITY_REQUEST_CODE)
        }else{ //si estamos en una pantalla grande
            title = list.name

            listFragment = ListDetailFragment.newInstance(list)

            listFragment?.let {
                supportFragmentManager.beginTransaction().replace(R.id.fragment_container
                ,it, getString(R.string.list_fragment_tag))
                    .addToBackStack(null)
                    .commit()
            }
            fab.setOnClickListener{
                showCreateTaskDialog()
            }
        }

    }
    fun showCreateTaskDialog(){
        val taskEditText = EditText(this)

        taskEditText.inputType = InputType.TYPE_CLASS_TEXT

        AlertDialog.Builder(this)
            .setTitle(R.string.add_task_title) //POSIBLE ERROR (solucionado)
            .setView(taskEditText)
            .setPositiveButton(R.string.add_task){
                dialog, _ -> val task = taskEditText.text.toString()
                listFragment?.addTask(task)
                dialog.dismiss()
            }
            .create()
            .show()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        title = resources.getString(R.string.app_name)

        //mandamosa guardar a la lista
        listFragment?.list?.let {
            listSelectionFragment.listDataManager.saveList(it)
        }
        //removemos de la derecha al fragment
        listFragment?.let {
            supportFragmentManager
                .beginTransaction()
                .remove(it)
                .commit()
            listFragment = null
        }
        //para que muestre una nueva tarea de la lista
        fab.setOnClickListener{
            showDialog()
        }

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) { //es cuando regresamos al activity presionando el boton back
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == LIST_DETAIL_ACTIVITY_REQUEST_CODE && resultCode == Activity.RESULT_OK){
            data?.let {
                //esto solo se va a ejecutar si el data no es null (para eso sirve el let)
                listSelectionFragment.saveList(data.getParcelableExtra(INTENT_LIST_KEY))
            }
        }
    }




    companion object {
        const val INTENT_LIST_KEY = "list"
        const val LIST_DETAIL_ACTIVITY_REQUEST_CODE = 123
    }
}
